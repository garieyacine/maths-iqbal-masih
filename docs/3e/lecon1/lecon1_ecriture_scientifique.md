---
author: ygarie
title: Leçon 1 - Ecriture scientifique
---
## I - Cours 

??? note "Leçon : Notation scientifique"
     ![Texte2](lecon1_ecriture_scientifique_cours.png) 

## II - Exercices

### exercices d'application : Associer une écriture décimale à son écriture scientifique.

??? note "1. Donner l'ecriture décimale d'une puissance de 10"
     <iframe
      src="https://coopmaths.fr/alea/?uuid=93df9&id=4C30-2&n=4&d=10&s=3&alea=GWgt&cd=1&v=eleve&es=0111001&title=" 
      width="100%"
      height="500" 
      frameborder="0" 
      allowfullscreen>
      </iframe>


??? note "2. Donner la notation scientifique 1 "
     <iframe 
     src="https://coopmaths.fr/alea/?uuid=a0d16&id=4C32&n=3&d=10&s=1&s2=1&alea=aKaL&cd=1&uuid=a0d16&id=4C32&n=3&d=10&s=1&s2=3&alea=aMPT&cd=1&v=eleve&es=0111001&title=" 
     width="100%" 
     height="500" 
     frameborder="0" 
     allowfullscreen>
     </iframe>

??? note "3. Donner la notation scientifique 2 "
     <iframe 
     src="https://coopmaths.fr/alea/?uuid=762fe&id=4C32-1&alea=XXHK&v=eleve&es=0111001&title=" 
     width="100%" 
     height="500" 
     frameborder="0" 
     allowfullscreen>
     </iframe>

### exercices problèmes

Pré-requis : 4ème-Leçon  - Ecriture scientifique (préfixe des puissances de 10)


??? note "Exercice problème "
     <iframe 
     src="https://coopmaths.fr/alea/?uuid=051c7&id=4C32-3&n=2&d=10&alea=51gy&cd=1&v=eleve&es=0111001&title="
      width="100%" 
      height="100%" 
      frameborder="0" 
      allowfullscreen>
      </iframe>


## III - Révisions

### Ce qu'il faut retenir



### Flashcards

??? note "Flashcards"
    

### Ce qu'il faut savoir faire


