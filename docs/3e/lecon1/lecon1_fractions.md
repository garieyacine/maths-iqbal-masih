---
author: ygarie
title: Leçon 1 - Fractions
---
## I - Cours 

Pour addition et soustractions de fractions voir :  5ème Leçon 7 - Fractions

??? note "Leçon : Propriétés de calcul sur les fractions"
     ![Texte2](lecon1_fractions_cours.png) 


## II - Exercices

### Exercices d'application 

??? note "1. Rendre irreductible une fraction"
     <iframe 
     src="https://coopmaths.fr/alea/?uuid=f1208&id=can3C03&n=3&d=10&alea=T9AI&i=1&cd=1&v=eleve&es=0111001&title=" 
     width="100%" 
     height="500" 
     frameborder="0" 
     allowfullscreen>
     </iframe>


??? note "2. Additionner / Soustraire deux fractions "
     <iframe 
     src="https://coopmaths.fr/alea/?uuid=5f429&id=4C21&n=2&d=10&s=1&s2=false&s3=true&s4=true&alea=vIuv&cd=1&uuid=5f429&id=4C21&n=2&d=10&s=2&s2=false&s3=true&s4=false&alea=UCT7&cd=1&v=eleve&es=0111001&title=" 
     width="100%" 
     height="500" 
     frameborder="0" 
     allowfullscreen>
     </iframe>

??? note "3. Multiplier / Diviser deux fractions "
     <iframe 
     src="https://coopmaths.fr/alea/?uuid=72ce7&id=4C22&n=2&d=10&s=2&s2=false&s3=true&s4=1&alea=5WIr&cd=1&uuid=55354&id=4C22-2&n=2&d=10&s=2&alea=psos&cd=1&v=eleve&es=0111001&title=" 
     width="100%" 
     height="500" 
     frameborder="0" 
     allowfullscreen></iframe>

### exercices problèmes

??? note "Problème 1 : Le club potager d'Iqbal"
     <iframe 
     src="https://coopmaths.fr/alea/?uuid=d0fdc&id=2N30-7&n=1&d=10&s=4&alea=ys92&cd=1&v=eleve&es=0111001&title=" 
     width="100%" 
     height="500" 
     frameborder="0" 
     allowfullscreen>
     </iframe>

??? note "Problème 2 : Le Yohaku additif"
     <iframe src="https://coopmaths.fr/alea/?uuid=1a61d&id=4C21-2&n=1&d=10&s=10&s2=1&s3=2&s4=true&alea=wYrO&cd=1&v=eleve&es=0111001&title=" width="100%" height="500" frameborder="0" allowfullscreen></iframe>




## III - Révisions

### Ce qu'il faut retenir


### Flashcards

??? note "Flashcards"
    

### Ce qu'il faut savoir faire