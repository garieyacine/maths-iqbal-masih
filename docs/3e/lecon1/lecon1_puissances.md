---
author: ygarie
title: Leçon 1 - Puissances
---
## I - Cours 

??? note "Leçon : Propriétés de calcul sur les puissances"
     ![Texte2](lecon1_puissances_cours.png) 





## II - Exercices

### exercices d'application : notion de puissances.

??? note "1. Déterminer le signe d'une puissance"
     <iframe 
     src="https://coopmaths.fr/alea/?uuid=67432&id=4C37&n=4&d=10&alea=GoYI&cd=1&v=eleve&es=0111001&title=" 
     width="100%"
      height="500"
      frameborder="0" 
      allowfullscreen>
      </iframe>


??? note "2. Calcul mental : Donner l'ecriture entière d'une puissance "
     <iframe
      src="https://coopmaths.fr/alea/?uuid=36f8b&id=4C30-3&alea=vMq1&v=eleve&es=0111001&title=" 
      width="100%" 
      height="500" 
      frameborder="0" 
      allowfullscreen>
      </iframe>

??? note "3. Simplifier des ecritures de puissances "
     <iframe 
     src="https://coopmaths.fr/alea/?uuid=bae57&id=4C33-1&n=4&d=10&s=1-2&s2=3&alea=69zm&cd=1&v=eleve&es=0111001&title=" 
     width="100%" 
     height="500" 
     frameborder="0" 
     allowfullscreen>
     </iframe>

### exercices problèmes


## III - Révisions

### Ce qu'il faut retenir


### Flashcards

??? note "Flashcards"
    

### Ce qu'il faut savoir faire