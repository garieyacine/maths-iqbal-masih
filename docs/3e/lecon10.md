---
author: ygarie
title: Leçon 10 - Fonctions affines, fonctions linéaires
---
## I - Cours 

??? note "Leçon : fonctions linéaires, fonctions affines"
     <div class="centre">
        <iframe 
        src="../pdf/lecon_fonctions2.pdf"
        width="1000" height="1000" 
        frameborder="0" 
        allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
        </iframe>
     </div>

## I - Exercices  

### Exercices d'application : expression littérale

??? note "Calculer images et antécédent à l'aide d'une expression littérale"
     <iframe
      height="400" 
      src="https://coopmaths.fr/alea/?uuid=aeb5a&id=3F20&n=4&d=10&s=1&s2=1-4&cd=1&uuid=20d20&id=3F20-1&n=4&d=10&s=1&s2=1-4&cd=1&v=eleve&es=021100&title="
      frameborder="0" >
     </iframe>


??? note "Determiner une fonction à l'aide d'un graphique : vocabulaire et notion"
     <iframe
      height="400" 
      src="https://coopmaths.fr/alea/?uuid=056fa&id=3F21-3&n=1&d=10&s=1&s2=3&s3=3&s4=1&cd=1&uuid=056fa&id=3F21-3&n=1&d=10&s=3&s2=3&s3=3&s4=2&cd=1&v=eleve&es=021100&title="
      frameborder="0" >
     </iframe>





### Exercices d'application : Representer une fonction affine.

??? note "à venir"

### Exercices problèmes

??? note "DNB 2023 - Métropole - ex 3"
     <iframe
      height="400" 
      src="https://coopmaths.fr/alea/?uuid=dnb_2023_09_metropole_3&alea=Yrtz&v=eleve&es=021100&title="
      frameborder="0" >
     </iframe>


## III - Révisions

### Ce qu'il faut retenir

- Un situation de proportionnalité est représentée par une fonction linéaire uniquement.
- Une fonction linéaire est une fonction affine particulière.
- La représentation graphique d'une fonction affine (et donc linéaire) est une droite.
- La représentation graphique d'une fonction linéaire est une droite qui passe par l'origine du repère (0,0)


### Flashcards

??? note "a venir"

### Ce qu'une personne en 3e doit **savoir faire**

- Elle représente graphiquement une fonction linéaire, une fonction affine.
- Elle interprète les paramètres d’une fonction affine suivant l’allure de sa courbe représentative.
- Elle modélise un phénomène continu par une fonction.
- Elle modélise une situation de proportionnalité à l’aide d’une fonction linéaire.
- Elle résout des problèmes modélisés par des fonctions en utilisant un ou plusieurs modes de représentation