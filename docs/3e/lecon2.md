---
author: ygarie
title: Leçon 2 - Homothétie et autres transformations
---
## I - Cours 

??? note "Leçon : Homothétie"
     ![Homothétie 3e](lecon2_homothetie_cours.png) 


## II - Exercices

### exercices d'application : 

??? note "1. Encadrer un rapport k"
    <iframe src="https://coopmaths.fr/alea/?uuid=6f383&id=3G13&n=2&d=10&s=10&s2=3&s3=1&s4=true&cd=1&alea=TH6X&v=eleve&es=0211001&title=" width="100%" height=500 frameborder="0" allowfullscreen></iframe>

??? note "2. Calculer un rapport k"
    <iframe src="https://coopmaths.fr/alea/?uuid=6f383&id=3G13&n=2&d=10&s=1&s2=3&s3=1&s4=true&cd=1&alea=TH6X&v=eleve&es=0211001&title=" width="100%" height=400 frameborder="0" allowfullscreen></iframe>

??? note "3. Calculer une longueur en deux étapes"
    <iframe src="https://coopmaths.fr/alea/?uuid=6f383&id=3G13&n=1&d=10&s=4&s2=3&s3=1&s4=true&cd=1&alea=TH6X&v=eleve&es=0211001&title=" width="100%" height=400 frameborder="0" allowfullscreen></iframe>

??? note "4. Calculer une aire"
    <iframe src="https://coopmaths.fr/alea/?uuid=6f383&id=3G13&n=2&d=10&s=6&s2=3&s3=1&s4=true&i=1&cd=1&alea=7jFg&v=eleve&es=0211001&title=" width="100%" height=400 frameborder="0" allowfullscreen></iframe>

??? note "5. Calculer un rapport k à l'aide des aires"
    <iframe src="https://coopmaths.fr/alea/?uuid=6f383&id=3G13&n=2&d=10&s=8&s2=3&s3=1&s4=true&cd=1&alea=7jFg&v=eleve&es=0211001&title=" width="100%" height=400 frameborder="0" allowfullscreen></iframe>

### exercices problèmes

??? note "PROBLEME : DNB - Exercice 6 Juin 1018 (Asie)"
    <iframe src="https://coopmaths.fr/alea/?uuid=dnb_2018_06_asie_6&v=eleve&es=0211001&title=" width="100%" height=500 frameborder="0" allowfullscreen></iframe>

## III - Révisions

### Ce qu'il faut retenir


### Flashcards


### Ce qu'il faut savoir faire

