---
author: ygarie
title: Leçon 3 - Calcul littéral
---
## I - Cours 

??? note "Leçon : Calcul littéral 1"
     ![Calcul littéral 1 - 3e](lecon3_calcul_litteral_cours.png) 

## II - Exercices

### exercices d'application : réduire une expression.

??? note "1. Supprimer les parenthèses"
     <iframe
      height="400" 
      src="https://coopmaths.fr/alea/?uuid=603a8&id=3L10&n=5&d=10&s=1-2-3-4&cd=1&v=eleve&es=021100&title="
      frameborder="0" >
     </iframe>

??? note "2. Supprimer les parenthèses et réduire"
     <iframe
      height="400" 
      src="https://coopmaths.fr/alea/?uuid=c88ba&id=3L10-2&n=3&d=10&s=5-6-7-8-9-10&cd=1&v=eleve&es=021100&title="
      frameborder="0" >
     </iframe>

### exercices d'application : Développement.

??? note "1. Simple distributivité"
     <iframe 
     src="https://coopmaths.fr/alea/?uuid=db2e0&id=3L11&n=4&d=10&s=3&s2=2&s3=1-4-5-6&s4=false&i=1&cd=1&alea=2Yv9&v=eleve&es=0211001&title=" 
     width="100%" 
     height=400 
     frameborder="0" 
     allowfullscreen>
     </iframe>



??? note "2. Double distributivité"
     <iframe
      height="400" 
      src="https://coopmaths.fr/alea/?uuid=4197c&id=3L11-1&n=4&d=10&s=3&s2=true&s3=false&cd=1&v=eleve&es=021100&title="
      frameborder="0" >
     </iframe>

### exercices d'applications : Factoriser.

??? note "1. Facteurs communs"
     <iframe
      height="400" 
      src="https://coopmaths.fr/alea/?uuid=5f5a6&id=3L11-4&n=6&d=10&s=4&cd=1&v=eleve&es=021100&title="
      frameborder="0" >
     </iframe>

??? note "2. Identité remarquable a²-b² = (a-b)(a+b) "
     <iframe
      height="400" 
      src="https://coopmaths.fr/alea/?uuid=81fd2&id=3L12&n=3&d=10&s=4&s2=true&cd=1&v=eleve&es=021100&title="
      frameborder="0" >
     </iframe>

### exercices d'applications : Résoudre une équation produit nul.

??? note "résoudre une équation produit nul"
     <iframe
      height="400" 
      src="https://coopmaths.fr/alea/?uuid=ecf62&id=3L14&n=4&d=10&s=8&cd=1&v=eleve&es=021100&title="
      frameborder="0" >
     </iframe>

### exercices problèmes

Pré-requis : 4ème-Leçon 4 - Résoudre une équation.


??? note "Exercice problème 1"
     <iframe
      width="100%"
      height="400" 
      src="https://coopmaths.fr/alea/?uuid=22412&id=3L13-3&n=1&d=10&s=14&s2=false&cd=1&v=eleve&es=021100&title="
      frameborder="0" >
     </iframe>

??? note "Exercice problème 2"
     <iframe
      height="400" 
      src="https://coopmaths.fr/alea/?uuid=cd2f2&id=3L13-4&n=1&d=10&s2=2&cd=0&v=eleve&es=021100&title="
      frameborder="0" >
     </iframe>

## III - Révisions

### Ce qu'il faut retenir

- Utiliser la lettre permet de **démontrer**.
- Metttre en équation permet de **résoudre des problèmes**.
- Développer c'est transformer un produit en somme algébrique.
- Factoriser c'est transformer une somme algébrique en produit.

### Flashcards

??? note "Flashcards"
    <iframe src="https://ladigitale.dev/digiflashcards/#/f/66155f6b2b8d8?vue=apprenant" allowfullscreen frameborder="0" width="100%" height="500"></iframe>

### Ce qu'il faut savoir faire

- Savoir développer. (simple et double distributivité)
- Savoir factoriser. (simple distributivité ou identité remarquable)
- Mettre un problème en équation.
- Résoudre une équation.
- Utiliser la lettre pour démontrer.
