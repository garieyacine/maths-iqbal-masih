---
author: ygarie
title: Leçon 4 - Statistiques
---
## I - Cours 

??? note "Leçon : Statistiques"
     ![Statistiques 3e](lecon4_statistiques_cours.png) 

## II - Exercices

### exercices d'application : 

??? note "1. Effectifs et fréquences"
    <iframe src="https://coopmaths.fr/alea/?uuid=f4b95&id=3S12&alea=HATT&v=eleve&es=0211001&title=" width="100%" height=400 frameborder="0" allowfullscreen></iframe>

??? note "2. Moyenne, médiane et étendue (liste)" 
    <iframe src="https://coopmaths.fr/alea/?uuid=b8afd&id=3S14&n=1&d=10&s=2&s2=4&s3=true&cd=1&alea=oLDs&v=eleve&es=0211001&title=" width="100%" height=400 frameborder="0" allowfullscreen></iframe>

??? note "3. Moyenne, médiane et étendue (tableau)" 
    <iframe src="https://coopmaths.fr/alea/?uuid=b8afd&id=3S14&n=1&d=10&s=4&s2=4&s3=true&cd=1&alea=oLDs&v=eleve&es=0211001&title=" width="100%" height=400 frameborder="0" allowfullscreen></iframe>

### exercices problèmes

Pré-requis : Voir BREVET

## III - Révisions

### Ce qu'il faut retenir


### Flashcards


### Ce qu'il faut savoir faire

- Calculer des effectif (fractions, pourcentages, écriture décimale)
- Calculer une moyenne
- Determiner la médiane
- Calculer une étendue

