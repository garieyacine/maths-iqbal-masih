---
author: ygarie
title: Leçon 5 - Thalès
---

## I - Cours 

??? note "Leçon : Thalès"
     ![Thalès 3e](lecon5_thales_cours.png) 

## II - Exercices

### exercices d'applications : Théorème.

#### 1. Triangles imbriqués

??? note "Théorème - triangles imbriqués"
    <iframe
        height="400" 
        src="https://coopmaths.fr/alea/?uuid=74eac&id=3G20&n=1&d=10&s=1&s2=2&cd=0&v=eleve&es=021100&title="
        frameborder="0" >
    </iframe>

#### 2. Configuration "papillon"

??? note "Théorème - papillons"
    <iframe
        height="400" 
        src="https://coopmaths.fr/alea/?uuid=74eac&id=3G20&n=1&d=10&s=2&s2=2&cd=0&v=eleve&es=021100&title="
        frameborder="0" >
    </iframe>

### exercices d'applications : Réciproque.

#### 1. Triangles imbriqués

??? note "Réciproque - triangle imbriqués"
    <iframe
      height="1000" 
      width="1000"
      src="https://coopmaths.fr/alea/?uuid=3451c&id=3G21&n=1&d=10&s=1&s2=3&s3=1&cd=1&v=eleve&es=021100&title="
      frameborder="0" >
    </iframe>

#### 2. Configuration "papillon"

??? note "Réciproque - papillons"
    <iframe
      height="400"
      width="400"
      src="https://coopmaths.fr/alea/?uuid=3451c&id=3G21&n=1&d=10&s=1&s2=3&s3=2&cd=1&v=eleve&es=011100&title="
      frameborder="0" >
    </iframe>

### exercices problèmes

??? note "Exercice problème"
    <div class="centre">
    <iframe
    height="400" 
    src="https://coopmaths.fr/alea/?uuid=eea67&id=3G20-1&v=eleve&es=011100&title="
    frameborder="0" >
    </iframe>
    </div>

## III - Révisions

### Ce qu'il faut retenir

- Le théorème sert à calculer des longueurs.
- La récprique du théorème de Thalès sert à montrer que deux droites sont parallèles.
- Dans une configuration de Thalès les longueurs du petit triangle sont proportionnelles à celles du grand triangle. (-->égalité des rapports)

### Flashcards

??? note "Flashcards"
    <iframe src="https://ladigitale.dev/digiflashcards/#/f/66154176a7bd0?vue=apprenant" allowfullscreen frameborder="0" width="100%" height="500"></iframe>

### Ce qu'il faut savoir faire

- Donner les conditions pour Thalès.
- Donner l'égalité des rapports de Thalès.
- Calculer une longueur à l'aide du produit en croix.
- Utiliser la réciproque du théorème de Thalès pour montrer que deux droites sont parallèles.