---
author: ygarie
title: Leçon 6 - Probabilités
---
## I - Cours 

??? note "Leçon : Probabilités"
     ![Probabilités 3e](lecon6_probabilites_cours.png) 

## II - Exercices

### exercices d'application : 

??? note "1. Vocabulaire"
    <iframe src="https://coopmaths.fr/alea/?uuid=7ba64&id=4S20&n=1&d=10&cd=1&alea=zmYt&v=eleve&es=0211001&title=" width="100%" height=400 frameborder="0" allowfullscreen></iframe>

??? note "2. Experience à une épreuve"
    <iframe src="https://coopmaths.fr/alea/?uuid=24db8&id=4S20-4&n=5&d=10&s=true&cd=1&alea=aOwf&v=eleve&es=0211001&title=" width="100%" height=400 frameborder="0" allowfullscreen></iframe>

??? note "3. Experience à deux épreuves"
    <iframe src="https://coopmaths.fr/alea/?uuid=76230&id=3S21&i=1&alea=Vkm5&v=eleve&es=0211001&title=" width="100%" height=400 frameborder="0" allowfullscreen></iframe>


### exercices problèmes

??? note "PROBLEME : Exercice 1 Brevet Métropole - Juillet 2024"
    <iframe src="https://coopmaths.fr/alea/?uuid=d11ae&id=3S20DNB&n=1&d=10&s=false&cd=1&alea=QVl5&v=eleve&es=0211001&title=" width="100%" height=500 frameborder="0" allowfullscreen></iframe>


## III - Révisions

### Ce qu'il faut retenir


### Flashcards


### Ce qu'il faut savoir faire

- Calculer des probabilités dans une expérience à une épreuve.
- Calculer des probabilités dans une expérience à deux épreuves.