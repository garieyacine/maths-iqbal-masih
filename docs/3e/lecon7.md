---
author: ygarie
title: Leçon 7 - Notion de fonction
---
## I - Cours 

??? note "Leçon : Notion de fonction"
     ![Fonctions 1 - 3e](lecon7_fonctions_cours.png) 

## II - Exercices

??? note "Exercice d'intro : Notion et vocabulaire"
     <iframe
      height="400" 
      width="1000"
      src="https://coopmaths.fr/alea/?uuid=77d18&id=3F1-act&n=10&d=10&s=2&cd=1&v=eleve&z=0.6&es=021100&title="
      frameborder="0" >
     </iframe>


??? note "Exercice d'intro 2 : Notion et vocabulaire"
     <iframe
      height="400" 
      src="https://coopmaths.fr/alea/?uuid=02116&id=3F12&v=eleve&es=021100&title="
      frameborder="0" >
     </iframe>


??? note "Lire image et antécédents dans un **tableau**"
     <iframe
      height="400" 
      src="https://coopmaths.fr/alea/?uuid=b92da&id=3F10&v=eleve&es=021100&title="
      frameborder="0" >
     </iframe>


??? note "Lire **graphiquement** les images et les antécédents"
     <iframe
      height="400" 
      src="https://coopmaths.fr/alea/?uuid=4b121&id=3F13-1&v=eleve&es=021100&title="
      frameborder="0" >
     </iframe>


??? note "Calculer l'image d'un nombre par sa forme algébrique"
     <iframe
      height="400" 
      src="https://coopmaths.fr/alea/?uuid=ba520&id=3F10-2&n=4&d=10&s=5&s2=1&s3=1&cd=0&v=eleve&es=021100&title="
      frameborder="0" >
     </iframe>


??? note "Calculer les antécédents d'un nombre par sa forme algébrique"
     <iframe
      height="400" 
      src="https://coopmaths.fr/alea/?uuid=ba520&id=3F10-2&n=3&d=10&s=2&s2=2&s3=1&cd=0&v=eleve&es=021100&title="
      frameborder="0" >
     </iframe>



## III - Révisions

### Ce qu'il faut retenir



### Flashcards



### Ce qu'une personne en 3e doit **savoir faire**

- Elle utilise les notations et le vocabulaire fonctionnels.  
- Elle passe d’un mode de représentation d’une fonction à un autre.
- Elle détermine, à partir de tous les modes de représentation, l’image d’un nombre.
- Elle détermine, à partir de tous les modes de représentation, un antécédent d’un nombre.
- Elle détermine de manière algébrique l’antécédent par une fonction, dans des cas se ramenant à la résolution d’une équation du premier degré.
