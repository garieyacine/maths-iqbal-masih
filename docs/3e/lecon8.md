---
author: ygarie
title: Leçon 8 - Arithmétique
---
## I - Cours 

??? note "Leçon : Arithmétique"
     ![Arithmétique 3e](lecon8_arithmetique_cours.png) 

## II - Exercices

### exercices d'application : 

??? note "1. Décomposer en facteurs premiers"
    <iframe src="https://coopmaths.fr/alea/?uuid=eee79&id=3A10-5&alea=DWOe&v=eleve&es=0211001&title=" width="100%" height=400 frameborder="0" allowfullscreen></iframe>

??? note "2. Rendre irreductible une fraction"
    <iframe src="https://coopmaths.fr/alea/?uuid=a6667&id=3A11&alea=seTd&v=eleve&es=0211001&title=" width="100%" height=400 frameborder="0" allowfullscreen></iframe>
    

??? note "3. PGCD"
    <iframe src="https://coopmaths.fr/alea/?uuid=cb844&id=3A10-8&n=1&d=10&s=6&cd=1&alea=Fc0k&v=eleve&es=0211001&title=" width="100%" height=400 frameborder="0" allowfullscreen></iframe>


### exercices problèmes


??? note "PROBLEME 1 : Plus petit multiple commun"
    <iframe src="https://coopmaths.fr/alea/?uuid=80772&id=3A11-1&n=1&d=10&s=2&s2=2&cd=1&alea=wlim&v=eleve&es=0211001&title=" width="100%" height=400 frameborder="0" allowfullscreen></iframe>

??? note "PROBLEME 2 : Plus grand diviseur commun"
    <iframe src="https://coopmaths.fr/alea/?uuid=8c05e&id=3A12-1&n=1&d=10&s=3&i=1&cd=1&alea=9ZQO&v=eleve&es=0211001&title=" width="100%" height="100%" frameborder="0" allowfullscreen></iframe>


## III - Révisions

### Ce qu'il faut retenir
- Pour chaque nombre entier positif supérieur à 2, il existe une unique décomposition en facteurs premiers
- Cette décomposition peut servir à :
    - Rendre irreductible des fractions
    - Trouver le plus grand diviseur commun
    - Trouver le plus petit muliple commun

### Flashcards

A venir

### Ce qu'il faut savoir faire

- Décomposer en facteur premier
- Rendre irreductible une fraction
- Trouver le plus grand diviseur commun
- Trouver le plus petit muliple commun

