---
author: ygarie
title: Leçon 9 - Utiliser les tiangles semblables
---
## I - Cours 

??? note "Leçon : Triangles semblables"
     ![Triangles semblables 3e](lecon9_triangles_semblables_cours.png) 

## II - Exercices

### exercices d'application : 

??? note "1. Triangles égaux"
    <iframe src="https://coopmaths.fr/alea/?uuid=91513&id=3G23&alea=F44Z&v=eleve&es=0211001&title=" width="100%" height=400 frameborder="0" allowfullscreen></iframe>

??? note "2. Determiner angles et côtés homologues"
    <iframe src="https://coopmaths.fr/alea/?uuid=f4b7e&id=3G24-1&n=1&d=10&s=1&cd=1&alea=sjfW&v=eleve&es=0211001&title=" width="100%" height=400 frameborder="0" allowfullscreen></iframe>

??? note "3. Démontrer semblables"
    <iframe src="https://coopmaths.fr/alea/?uuid=f4b7e&id=3G24-1&n=2&d=10&s=2-3&cd=1&alea=sjfW&v=eleve&es=0211001&title=" width="100%" height=500 frameborder="0" allowfullscreen></iframe>


??? note "4. Calculer des longueurs"
    <iframe src="https://coopmaths.fr/alea/?uuid=58a64&id=3G24-2&n=1&d=10&s=3&cd=1&alea=Vckp&v=eleve&es=0211001&title=" width="100%" height=400 frameborder="0" allowfullscreen></iframe>


### exercices problèmes

??? note "PROBLEME 1 : Semblables ?"
    <iframe src="https://coopmaths.fr/alea/?uuid=f4b7e&id=3G24-1&n=1&d=10&s=4&cd=1&alea=sjfW&v=eleve&es=0211001&title=" width="100%" height=400 frameborder="0" allowfullscreen></iframe>
 
??? note "PROBLEME 2 : Semblables ?"
    <iframe src="https://coopmaths.fr/alea/?uuid=f4b7e&id=3G24-1&n=1&d=10&s=5&cd=1&alea=sjfW&v=eleve&es=0211001&title=" width="100%" height=400 frameborder="0" allowfullscreen></iframe>


Pré-Requis : Leçon TRIGONOMETRIE (pour la question 4)
??? note "PROBLEME 3 : DNB Juin 2018 Métropole - Ex4"
    <iframe src="https://coopmaths.fr/alea/?uuid=dnb_2018_06_metropole_4&v=eleve&es=0211001&title=" width="100%" height=500 frameborder="0" allowfullscreen></iframe>

## III - Révisions

### Ce qu'il faut retenir


### Flashcards


### Ce qu'il faut savoir faire


