---
author: ygarie
title: 🏡 Accueil
---

# Bienvenue sur le site pour les mathématicien.ne.s d'IQBAL MASIH

Ce site est en cours de développement.

Vous trouverez pour l'instant quelques leçons de 3e.

En développement :  
- Entrainement au brevet 3e.  
- Cours de 6e.  
- Cours de 4e.  
- Cours de 5e.  

Il peut y avoir quelques bugs, n'hésitez pas à nous les faire remonter via Pronote, par mail ou directement au collège.

L'équipe Maths =)

